<?php
class Attribute 
{
    private $values;

    private $name;

    public function __construct()
    {
        $this->values = new SplObjectStorage();
    }

    public function getValues()
    {
        return $this->values;
    }

    public function addValue(Value $value)
    {
        if (!$this->values->contains($value)) {
            $this->values->attach($value);
        }
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
}
class Value 
{
    private $name;

    private $attribute;

    public function __construct(Attribute $attribute)
    {
        $attribute->addValue($this);
        $this->attribute = $attribute;
    }

    public function getAttribute()
    {
        return $this->attribute;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
}

$attribute = (new Attribute())->setName('color');

$valueGreen = (new Value($attribute))->setName('green');
$valueBlue  = (new Value($attribute))->setName('blue');


//attribute::values equals attribute->getValues()
print_r($attribute);
print_r($attribute->getValues());

//valuesBlue::attribute not equals valuesBlue->getValues() (missing "blue" Value Object attribute `name` and `attribute`)
print_r($valueBlue);
print_r($valueBlue->getAttribute());
?>
