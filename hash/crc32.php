#!/usr/bin/php
<?php

$serviceList = [
    '10.10.0.1',
    '10.10.0.2',
    '10.10.0.3',
    '10.10.0.4',
];

$serviceCount = 4;

if ($argc <= 1)
{
    exit("Usage crc32.php param1 ...\n");
}

$params = $argv;
array_shift($params);

printf("Value\tHost\n");

foreach ($params as $item) {
    echo sprintf(
        "[%s]\t[%s]\n",
        $item,
        $serviceList[crc32($item)%$serviceCount]
    );
}
