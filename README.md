### 算法编码训练

通过编码实践，了解相关算法实现

### 训练提纲
- PHP
    - php.net
- Hash
    - Crc32
- Algorithm
    - Finite State Machine
- Data Structure
    - Todo

### Vision

Inspire PHP Design Pattern
